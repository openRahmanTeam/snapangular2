import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs/Rx';
import { Teacher } from '../model/teacher.model';
import { TeacherService } from '../service/teacher.service';
import { ProgressIndicatorService } from '../../../services/progress-indicator.service';
import { ActivatedRoute } from '@angular/router/src/router_state';

@Component({
  selector: 'app-teacherlist',
  templateUrl: './teacherlist.component.html',
  styleUrls: ['./teacherlist.component.css']
})
export class TeacherlistComponent implements OnInit {

  list: Teacher[];
  teacher1: any = {};
  public teachers: Observable<Teacher[]>;
  public teacher: any;
  public subscription: Subscription;

  constructor(
    private teacherService: TeacherService,
    private router: Router,
    private progress: ProgressIndicatorService) {
    this.subscription = this.teacherService.changeDetector()
      .subscribe(value => {
        if (value) {
          this.teacherService.get();
        }
      });
      // this.teacherService.get();
  }

  private handleError(errors: any): void {
    console.log('Terjadi error : ' + errors);
  }

  ngOnInit() {
    this.teacher1 = {};
    this.teachers = this.teacherService.teachers;
    this.getlist();
    // this.teacherService.get();
  }

  ngOnChange() {
    this.teacherService.get();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  logger(val) { console.log(val); }

  getlist() {
    this.teacherService.getTeacher()
      .subscribe(result => this.list = result,
      err => {
        console.log(err);
      });
  }

  delete(teacher: Teacher, id: string) {
    console.log('delele id: ' + id);
    if (confirm('Are you sure want to delete ' + teacher.name) + '?') {
      this.teacherService.delete(teacher, id)
        .subscribe(
        () => {
          this.progress.toggleIndicator(null);
          this.deleteNotification();
        },
        err => {
          console.log(err);
          alert('Could not delete teacher');
        }
        );
    }

  }

  deleteNotification() {
    this.progress.toggleIndicator('Data has been deleted');
    setTimeout(() => this.progress.toggleIndicator(null), 3000);
    this.getlist();
  }

}
