import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { Observable, BehaviorSubject, Subject } from 'rxjs/Rx';
import { Teacher } from './../model/teacher.model';
import { AuthService } from '../../../services/auth.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class TeacherService {

  private urlTeacher = 'api/teacher';
  private headers = new Headers();
  public teachers: Observable<Teacher[]>;
  public _teachers: BehaviorSubject<Teacher[]>;
  public datastore: { teachers: Teacher[] };
  public _changeDetector: Subject<boolean> = new Subject<boolean>();

  constructor(private http: Http,
    private authService: AuthService) {
    this.datastore = { teachers: [] };
    this._teachers = <BehaviorSubject<Teacher[]>>new BehaviorSubject([]);
    this.teachers = this._teachers.asObservable();
  }

  getTeacher(): Observable<Teacher[]> {
    this.setHeader(this.headers);
    console.log('url '+this.urlTeacher);
    return this.http.get(this.urlTeacher, { headers: this.headers })
      .map(result => result.json() as Teacher[])
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  get() {
    this.setHeader(this.headers);
    return this.http.get(this.urlTeacher, { headers: this.headers })
      .map((result: Response) => result.json())
      .subscribe(data => {
        if (data.length === 0) {
          console.log('Data not found.');
        }
        this.datastore.teachers = data;
        this._teachers.next(Object.assign({}, this.datastore).teachers);
      });
  }

  getOne(id: string) {
    this.setHeader(this.headers);
    return this.http.get(this.urlTeacher + '/id=' + id, { headers: this.headers })
      .map(result => result.json());
  }

  save(teacher: Teacher) {
    const options = this.setHeaderOption(teacher);
    return this.http.post(this.urlTeacher, teacher, options)
      .map((res: Response) => res.json())
      .subscribe(data => {
        if (typeof data.error !== 'undefined') {
          return;
        }
        console.log('success save');
        // this.datastore.permissions.push(permission);
        this._teachers.next(Object.assign({}, this.datastore).teachers);
        this._changeDetector.next(true);
      }, error => Observable.throw('failed save data.'));
  }

  update(teacher: Teacher, id: string) {
    const options = this.setHeaderOption(teacher);
    return this.http.put(this.urlTeacher + '/id=' + id, teacher, options)
      .map((res: Response) => res.json())
      .subscribe(data => {
        if (typeof data.error !== 'undefined') {
          return;
        }
        console.log('success update data');
        this._teachers.next(Object.assign({}, this.datastore).teachers);
        this._changeDetector.next(true);
      }, error => Observable.throw('failed update data'));
    // .catch((error: any) => Observable.throw('failed update'));
  }

  delete(teacher: Teacher, id: string): Observable<Teacher[]> {
    const options = this.setHeaderOption(teacher);
    return this.http.put(this.urlTeacher + '/delete/id=' + id, teacher, options)
      .map((res: Response) => res)
      .catch((error: any) => Observable.throw('failed delete'));
  }

  private setHeader(headers: Headers) {
    headers.append("Authorization", "Bearer " + this.authService.getCurrentUser().token);
  }

  private setHeaderOption(teacher: Teacher): any {
    const bodyString = JSON.stringify(teacher);
    const headers = new Headers({ 'application-Type': 'application/json' });
    headers.append("Authorization", "Bearer " + this.authService.getCurrentUser().token);
    return new RequestOptions({ headers: headers }); // Create a request option
  }

  changeDetector(): Observable<any> {
    return this._changeDetector.asObservable();
  }

}
