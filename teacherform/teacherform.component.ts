import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { TeacherService } from '../service/teacher.service';

@Component({
  selector: 'app-teacherform',
  templateUrl: './teacherform.component.html',
  styleUrls: ['./teacherform.component.css']
})
export class TeacherformComponent implements OnInit {

  title: string;
  public id: string;
  public teacherForm: FormGroup;
  public teacher: any = {};
  public sub: any;

  constructor(
    public activatedRoute: ActivatedRoute,
    public teacherService: TeacherService,
    public formBuilder: FormBuilder,
    public router: Router) { }

  ngOnInit() {
    this.teacherForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['']
    });

    this.sub = this.activatedRoute.params.subscribe(param => {
      this.id = param['id'];
    });

    this.title = this.id ? 'Edit Form' : 'New Form';

    if (this.id) {
      this.teacherService.getOne(this.id)
        .subscribe(teacher => {
          this.teacher = teacher;
          console.log(this.teacher.info.teacher.name);
          this.teacher.name = this.teacher.info.teacher.name;
          this.teacher.email = this.teacher.info.teacher.email;
        },
        response => {
          console.log(this.teacher);
        });
    }
  }

  onSubmit() {
    if (this.id) {
      console.log(this.teacherForm.value);
      this.teacherService.update(this.teacherForm.value, this.id);
    } else {
      this.teacherService.save(this.teacherForm.value);
    }
    this.router.navigate(['/content/teacher']);
  }

}
