export class Teacher {
    public id: string;
    public name: string;
    public email: string;
}
